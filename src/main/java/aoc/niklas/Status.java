package aoc.niklas;

public class Status {
  public Boolean Incomplete;
  public Integer SyntaxErrorPoints;

  public Status(Integer points, Boolean incomplete) {
    SyntaxErrorPoints = points;
    Incomplete = incomplete;
  }
}
