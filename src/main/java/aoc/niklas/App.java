package aoc.niklas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Hello world!
 */
public final class App {
    private App() {
    }

    /**
     * Says hello to the world.
     * @param args The arguments of the program.
     * @throws IOException
     * @throws URISyntaxException
     */
    public static void main(String[] args) throws IOException, URISyntaxException {
      // Open the file
      URL resource = App.class.getClassLoader().getResource("input.txt");
      File file = new File(resource.toURI());
      FileInputStream input = new FileInputStream(file);
      BufferedReader br = new BufferedReader(new InputStreamReader(input));

      String strLine;
      ArrayList<String> inputLines = new ArrayList<String>();
      //Read File Line By Line
      while ((strLine = br.readLine()) != null)   {
        inputLines.add(strLine);
      }
      input.close();

      Integer syntaxErrorPoints = 0;
      ArrayList<Long> autoCompleteScores = new ArrayList<Long>();
      NavigationHandler navHandler = new NavigationHandler();
      for (String str : inputLines)  {
        Status status = navHandler.ControlLine(str);
        if(!status.Incomplete){
          syntaxErrorPoints += status.SyntaxErrorPoints;
        } else {
          autoCompleteScores.add(navHandler.GetAutoCompleteScore(str));
        }
      }

      Collections.sort(autoCompleteScores);
      Long middleValue = autoCompleteScores.get((int)Math.floor(autoCompleteScores.size() / 2));
      System.out.println("Syntax Error Score: " + syntaxErrorPoints);
      System.out.println("Middle Auto Complete Score: " + middleValue);

    }
}
