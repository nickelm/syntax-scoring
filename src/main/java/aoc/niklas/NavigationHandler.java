package aoc.niklas;

import java.util.Stack;

public class NavigationHandler {

  public NavigationHandler(){
  }

  public Status ControlLine(String str){
      String tmpStr = str;
      Chunk chunk = new Chunk(0);
      while(tmpStr.length() > 0){
        chunk = ValidateChunk(tmpStr);
        if(chunk == null){
          // Incomplete line, no points
          return new Status(0, true);
        } else if(chunk.IllegalChar != null) {
          return new Status(GetPointsOfChar(chunk.IllegalChar), false);
        } else {
          tmpStr = tmpStr.substring(chunk.Length);
        }
      }
      return new Status(0, false);
  }

  public Long GetAutoCompleteScore(String str){
    Stack<Character> chars = new Stack<Character>();
    for (int i = 0; i < str.length(); i++) {
      Character c = str.charAt(i);
      if(IsStartChar(c)){
        chars.push(c);
      } else {
        chars.pop();
      }
    }
    return CalculateStack(chars);
  }

  private Long CalculateStack(Stack<Character> chars){
    Long score = (long)0;
    while(chars.size() > 0){
      score = score * 5 + GetScoreForAutoCompleteChar(GetEndChar(chars.pop()));
    }
    return score;
  }

  private Integer GetScoreForAutoCompleteChar(Character c){
    if(c == ')') return 1;
    if(c == ']') return 2;
    if(c == '}') return 3;
    if(c == '>') return 4;

    return 0;
  }

  private Chunk ValidateChunk(String str){
    if(str.length() == 0){
      return null;
    }

    Character startChar = str.charAt(0);
    for (int i = 1; i < str.length(); i++) {
      if(str.charAt(i) == GetEndChar(startChar)){
        //Correct endchar, return the found Chunk
        return new Chunk(i + 1);
      } else if(IsEndChar(str.charAt(i))) {
        //Has to be other end char, return Chunk ending with illegal char
        Chunk chunk = new Chunk(i + 1);
        chunk.IllegalChar = str.charAt(i);
        return chunk;
      } else if(IsStartChar(str.charAt(i))) {
        String ch = str.substring(i);
        Chunk innerChunk = ValidateChunk(ch);
        if(innerChunk == null || innerChunk.IllegalChar != null){
          // We just return the corrupt or incomplete chunk to pass it up the recursion
          return innerChunk;
        }
        i = i + innerChunk.Length - 1;
      }
    }
    // We didn't fine an end, so the Chunk is not complete
    return null;
  }

  private char GetEndChar(char startChar){
    if(startChar == '{') return '}';
    if(startChar == '(') return ')';
    if(startChar == '<') return '>';
    if(startChar == '[') return ']';

    throw new IllegalArgumentException();
  }

  private Boolean IsEndChar(char currentChar){
    char[] endChars = new char[] {'}',')','>',']'};
    for (char c : endChars) {
      if(c == currentChar){
        return true;
      }
    }
    return false;
  }

  private Boolean IsStartChar(char currentChar){
    char[] startChars = new char[] {'{','(','<','['};
    for (char c : startChars) {
      if(c == currentChar){
        return true;
      }
    }
    return false;
  }

  private Integer GetPointsOfChar(char illegalChar){
    if(illegalChar == ')') return 3;
    if(illegalChar == ']') return 57;
    if(illegalChar == '}') return 1197;
    if(illegalChar == '>') return 25137;

    return 0;
  }
}
