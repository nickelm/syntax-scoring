package aoc.niklas;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Unit test for simple App.
 */
class NavigationTest {
    /**
     * Rigorous Test.
     */
    @Test
    void navigationTest() {

      ArrayList<String> Data = new ArrayList<String>();

      Data.add("[({(<(())[]>[[{[]{<()<>>");
      Data.add("[(()[<>])]({[<{<<[]>>(");
      Data.add("{([(<{}[<>[]}>{[]{[(<()>");
      Data.add("(((({<>}<{<{<>}{[]{[]{}");
      Data.add("[[<[([]))<([[{}[[()]]]");
      Data.add("[{[{({}]{}}([{[{{{}}([]");
      Data.add("{<[[]]>}<{[{[{[]{()[[[]");
      Data.add("[<(<(<(<{}))><([]([]()");
      Data.add("<{([([[(<>()){}]>(<<{{");
      Data.add("<{([{{}}[<[[[<>{}]]]>[]]");
      
      NavigationHandler navHandler = new NavigationHandler();

      Status line1Status = navHandler.ControlLine(Data.get(0));
      assertEquals(line1Status.Incomplete, true);
      assertEquals(line1Status.SyntaxErrorPoints, 0);

      Status line2Status = navHandler.ControlLine(Data.get(1));
      assertEquals(line2Status.Incomplete, true);
      assertEquals(line2Status.SyntaxErrorPoints, 0);

      Status line3Status = navHandler.ControlLine(Data.get(2));
      assertEquals(line3Status.Incomplete, false);
      assertEquals(line3Status.SyntaxErrorPoints, 1197);

      Status line4Status = navHandler.ControlLine(Data.get(3));
      assertEquals(line4Status.Incomplete, true);
      assertEquals(line4Status.SyntaxErrorPoints, 0);

      Status line5Status = navHandler.ControlLine(Data.get(4));
      assertEquals(line5Status.Incomplete, false);
      assertEquals(line5Status.SyntaxErrorPoints, 3);

      Status line6Status = navHandler.ControlLine(Data.get(5));
      assertEquals(line6Status.Incomplete, false);
      assertEquals(line6Status.SyntaxErrorPoints,57);

      Status line7Status = navHandler.ControlLine(Data.get(6));
      assertEquals(line7Status.Incomplete, true);
      assertEquals(line7Status.SyntaxErrorPoints, 0);

      Status line8Status = navHandler.ControlLine(Data.get(7));
      assertEquals(line8Status.Incomplete, false);
      assertEquals(line8Status.SyntaxErrorPoints, 3);

      Status line9Status = navHandler.ControlLine(Data.get(8));
      assertEquals(line9Status.Incomplete, false);
      assertEquals(line9Status.SyntaxErrorPoints, 25137);

      Status line10Status = navHandler.ControlLine(Data.get(9));
      assertEquals(line10Status.Incomplete, true);
      assertEquals(line10Status.SyntaxErrorPoints, 0);
    }

    @Test
    void autoCompletionTest() {

      ArrayList<String> Data = new ArrayList<String>();

      Data.add("[({(<(())[]>[[{[]{<()<>>");
      Data.add("[(()[<>])]({[<{<<[]>>(");
      Data.add("(((({<>}<{<{<>}{[]{[]{}");
      Data.add("{<[[]]>}<{[{[{[]{()[[[]");
      Data.add("<{([{{}}[<[[[<>{}]]]>[]]");

      NavigationHandler navHandler = new NavigationHandler();
      ArrayList<Long> scores = new ArrayList<Long>();

      Long score1 = navHandler.GetAutoCompleteScore(Data.get(0));
      assertEquals(score1, 288957);
      scores.add(score1);

      Long score2 = navHandler.GetAutoCompleteScore(Data.get(1));
      assertEquals(score2, 5566);
      scores.add(score2);

      Long score3 = navHandler.GetAutoCompleteScore(Data.get(2));
      assertEquals(score3, 1480781);
      scores.add(score3);

      Long score4 = navHandler.GetAutoCompleteScore(Data.get(3));
      assertEquals(score4, 995444);
      scores.add(score4);

      Long score5 = navHandler.GetAutoCompleteScore(Data.get(4));
      assertEquals(score5, 294);
      scores.add(score5);

      Collections.sort(scores);
      Long middleValue = scores.get((int)Math.floor(scores.size() / 2));
      assertEquals(middleValue, 288957);
    }
}
